package com.dot.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
public class ServiceAApplication extends SpringBootServletInitializer{
	
	
	 @Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	        return application.sources(ServiceAApplication.class);
	    }

	public static void main(String[] args) {
		SpringApplication.run(ServiceAApplication.class, args);
	}
	
	
	

}

